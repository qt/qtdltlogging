// Copyright (C) 2021 The Qt Company Ltd.
// Copyright (C) 2019 Luxoft Sweden AB
// Copyright (C) 2018 Pelagicore AG
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only

#ifndef QDLTLOGGINGGLOBAL_H
#define QDLTLOGGINGGLOBAL_H

#include <QtCore/qglobal.h>
#include <QtDltLogging/qtdltlogging-config.h>

QT_BEGIN_NAMESPACE

#if defined(QT_BUILD_DLTLOGGING_LIB)
#  define Q_DLTLOGGING_EXPORT Q_DECL_EXPORT
#else
#  define Q_DLTLOGGING_EXPORT Q_DECL_IMPORT
#endif

QT_END_NAMESPACE

#endif // QDLTLOGGINGGLOBAL_H
