

#### Inputs



#### Libraries

qt_find_package(DLT PROVIDED_TARGETS DLT::DLT MODULE_NAME dltlogging QMAKE_LIB dlt)


#### Tests



#### Features

qt_feature("dlt" PRIVATE
    LABEL "DLT"
    AUTODETECT UNIX
    CONDITION DLT_FOUND AND DLT_VERSION VERSION_GREATER_EQUAL 2.12
)
qt_feature("dltlogging" PUBLIC
    LABEL "Qt DLT Logging"
    CONDITION QT_FEATURE_dlt
)
qt_configure_add_summary_section(NAME "Qt DLT Logging")
qt_configure_add_summary_entry(ARGS "dlt")
qt_configure_end_summary_section() # end of "Qt DLT Logging" section
qt_configure_add_summary_entry(
    ARGS "dltlogging"
    CONDITION NOT QT_FEATURE_dltlogging
)
qt_configure_add_report_entry(
    TYPE NOTE
    MESSAGE "No DLT libs found. Disabled building Qt DLT Logging."
    CONDITION NOT QT_FEATURE_dlt
)
