// Copyright (C) 2021 The Qt Company Ltd.
// Copyright (C) 2019 Luxoft Sweden AB
// Copyright (C) 2018 Pelagicore AG
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause

#include <QCoreApplication>
#include <QTimer>
#include <QtDltLogging/QtDlt>

#include <iostream>

#include "loggingcategories.h"

int main(int argc, char *argv[])
{
//![1]
    qInstallMessageHandler(QDltRegistration::messageHandler);
//![1]

    QCoreApplication a(argc, argv);

    QTimer timer;
    timer.connect(&timer, &QTimer::timeout, [] {
        static int counter = 0;
        counter++;
        qCCritical(FOO) << "FOO CATEGORY" << counter;
        qCWarning(BAR) << "BAR CATEGORY" << counter;
        qCritical() << "FALLBACK" << counter;
        if (FOO().isDebugEnabled()) {
            std::cout << "LONG TAKING OPERATION ONLY ENABLED IN DEBUG" << std::endl;
            qCDebug(FOO) << "Debug Statement" << counter;
        }
    });
    timer.setInterval(1000);
    timer.start();

    return a.exec();
}
