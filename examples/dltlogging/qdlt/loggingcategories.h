// Copyright (C) 2021 The Qt Company Ltd.
// Copyright (C) 2019 Luxoft Sweden AB
// Copyright (C) 2018 Pelagicore AG
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause

#ifndef LOGGINGCATEGORIES_H
#define LOGGINGCATEGORIES_H

#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(FOO)
Q_DECLARE_LOGGING_CATEGORY(BAR)

#endif // LOGGINGCATEGORIES_H
