// Copyright (C) 2021 The Qt Company Ltd.
// Copyright (C) 2019 Luxoft Sweden AB
// Copyright (C) 2018 Pelagicore AG
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause

#include "loggingcategories.h"
#include <QtDltLogging/QtDlt>

//![1]
QDLT_REGISTER_APPLICATION("APP1", "Description for APP")
//![1]

//![2]
QDLT_LOGGING_CATEGORY(FOO, "com.pelagicore.foo", "FOO", "FOO CATEGORY")
QDLT_LOGGING_CATEGORY(BAR, "com.pelagicore.bar", "BAR", "BAR CATEGORY")

QDLT_FALLBACK_CATEGORY(FOO)
//![2]
